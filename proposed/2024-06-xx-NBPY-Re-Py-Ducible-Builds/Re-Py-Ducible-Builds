Proposed for North Bay Python 2024: https://2024.northbaypython.org/

Re-Py-ducible Builds

This talk will demonstrate the whys, whats and hows of Reproducible
Builds by highlighting various issues identified in Python projects,
from the simple to the seemingly inscrutible.

Wait, what is Reproducible Builds, you say? Well, it is basically the
crazy idea that when you build something, and you build it again, you
get the exact same thing... or even more important, if someone else
builds it, they get the exact same thing too.

What good is it? Secure supply chains? Might help with that. Code
provenance? Yup. Finding really weird quirky obscure bugs? You know
it.

Through a few simple code examples demonstrating common problems folks
will learn things to watch out for when coding and how to fix them
once identified.

Speaking of fixing issues, some of the useful tooling for diagnosing
and troubleshooting reproducibility issues... just so happen to be
written in Python! Will even show you how to use them... and maybe
take a look inside.

Crank your Python best practices up to 11 with Reproducible Builds!
